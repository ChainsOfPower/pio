<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Organization
 *
 * @ORM\Table(name="organization")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrganizationRepository")
 */
class Organization
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location", inversedBy="organizations")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $location;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User", mappedBy="organization", cascade={"remove"})
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Laboratory", mappedBy="organization")
     */
    private $laboratories;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Organization
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Organization
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Organization
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set location
     *
     * @param \AppBundle\Entity\Location $location
     *
     * @return Organization
     */
    public function setLocation(\AppBundle\Entity\Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \AppBundle\Entity\Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Add laboratory
     *
     * @param \AppBundle\Entity\Laboratory $laboratory
     *
     * @return Organization
     */
    public function addLaboratory(\AppBundle\Entity\Laboratory $laboratory)
    {
        $this->laboratories[] = $laboratory;

        return $this;
    }

    /**
     * Remove laboratory
     *
     * @param \AppBundle\Entity\Laboratory $laboratory
     */
    public function removeLaboratory(\AppBundle\Entity\Laboratory $laboratory)
    {
        $this->laboratories->removeElement($laboratory);
    }

    /**
     * Get laboratories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLaboratories()
    {
        return $this->laboratories;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
