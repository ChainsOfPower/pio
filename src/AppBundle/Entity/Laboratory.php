<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Laboratory
 *
 * @ORM\Table(name="laboratory")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LaboratoryRepository")
 */
class Laboratory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location", inversedBy="laboratories")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Organization", inversedBy="laboratories", cascade={"remove"})
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id")
     */
    private $organization;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Purpose", inversedBy="laboratories")
     * @ORM\JoinColumn(name="purpose_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $purpose;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Hardware", mappedBy="laboratory")
     */
    private $hardwares;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Laboratory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Laboratory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set location
     *
     * @param \AppBundle\Entity\Location $location
     *
     * @return Laboratory
     */
    public function setLocation(\AppBundle\Entity\Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \AppBundle\Entity\Location
     */
    public function getLocation()
    {
        return $this->location;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->hardwares = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add hardware
     *
     * @param \AppBundle\Entity\Hardware $hardware
     *
     * @return Laboratory
     */
    public function addHardware(\AppBundle\Entity\Hardware $hardware)
    {
        $this->hardwares[] = $hardware;

        return $this;
    }

    /**
     * Remove hardware
     *
     * @param \AppBundle\Entity\Hardware $hardware
     */
    public function removeHardware(\AppBundle\Entity\Hardware $hardware)
    {
        $this->hardwares->removeElement($hardware);
    }

    /**
     * Get hardwares
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHardwares()
    {
        return $this->hardwares;
    }

    /**
     * Set organization
     *
     * @param \AppBundle\Entity\Organization $organization
     *
     * @return Laboratory
     */
    public function setOrganization(\AppBundle\Entity\Organization $organization = null)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return \AppBundle\Entity\Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set purpose
     *
     * @param \AppBundle\Entity\Purpose $purpose
     *
     * @return Laboratory
     */
    public function setPurpose(\AppBundle\Entity\Purpose $purpose = null)
    {
        $this->purpose = $purpose;

        return $this;
    }

    /**
     * Get purpose
     *
     * @return \AppBundle\Entity\Purpose
     */
    public function getPurpose()
    {
        return $this->purpose;
    }
}
