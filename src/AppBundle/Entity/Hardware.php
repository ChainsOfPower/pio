<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Hardware
 *
 * @ORM\Table(name="hardware")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HardwareRepository")
 */
class Hardware
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="contactPerson", type="string", length=128, nullable=true)
     */
    private $contactPerson;

    /**
     * @var string
     *
     * @ORM\Column(name="manufacturer", type="string", length=128, nullable=true)
     */
    private $manufacturer;

    /**
     * @var string
     * @ORM\Column(name="serialNumber", type="string", length=128)
     */
    private $serialNumber;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Laboratory", inversedBy="hardwares", cascade={"remove"})
     * @ORM\JoinColumn(name="laboratory_id", referencedColumnName="id")
     */
    private $laboratory;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Owner", inversedBy="hardwares")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category", inversedBy="hardwares")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\HardwareSoftware", mappedBy="hardwareId", cascade={"remove"})
     */
    private $softwares;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Hardware
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Hardware
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set contactPerson
     *
     * @param string $contactPerson
     *
     * @return Hardware
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * Get contactPerson
     *
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Set manufacturer
     *
     * @param string $manufacturer
     *
     * @return Hardware
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return string
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Set serialNumber
     *
     * @param string $serialNumber
     *
     * @return Hardware
     */
    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    /**
     * Get serialNumber
     *
     * @return string
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * Set laboratory
     *
     * @param \AppBundle\Entity\Laboratory $laboratory
     *
     * @return Hardware
     */
    public function setLaboratory(\AppBundle\Entity\Laboratory $laboratory = null)
    {
        $this->laboratory = $laboratory;

        return $this;
    }

    /**
     * Get laboratory
     *
     * @return \AppBundle\Entity\Laboratory
     */
    public function getLaboratory()
    {
        return $this->laboratory;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->owner = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add owner
     *
     * @param \AppBundle\Entity\Owner $owner
     *
     * @return Hardware
     */
    public function addOwner(\AppBundle\Entity\Owner $owner)
    {
        $this->owner[] = $owner;

        return $this;
    }

    /**
     * Remove owner
     *
     * @param \AppBundle\Entity\Owner $owner
     */
    public function removeOwner(\AppBundle\Entity\Owner $owner)
    {
        $this->owner->removeElement($owner);
    }

    /**
     * Get owner
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return Hardware
     */
    public function setCategory(\AppBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\Owner $owner
     *
     * @return Hardware
     */
    public function setOwner(\AppBundle\Entity\Owner $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Add software
     *
     * @param \AppBundle\Entity\HardwareSoftware $software
     *
     * @return Hardware
     */
    public function addSoftware(\AppBundle\Entity\HardwareSoftware $software)
    {
        $this->softwares[] = $software;

        return $this;
    }

    /**
     * Remove software
     *
     * @param \AppBundle\Entity\HardwareSoftware $software
     */
    public function removeSoftware(\AppBundle\Entity\HardwareSoftware $software)
    {
        $this->softwares->removeElement($software);
    }

    /**
     * Get softwares
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSoftwares()
    {
        return $this->softwares;
    }
}
