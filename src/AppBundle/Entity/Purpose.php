<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Purpose
 *
 * @ORM\Table(name="purpose")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PurposeRepository")
 */
class Purpose
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Laboratory", mappedBy="purpose")
     */
    private $laboratories;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Purpose
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Purpose
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->laboratories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add laboratory
     *
     * @param \AppBundle\Entity\Laboratory $laboratory
     *
     * @return Purpose
     */
    public function addLaboratory(\AppBundle\Entity\Laboratory $laboratory)
    {
        $this->laboratories[] = $laboratory;

        return $this;
    }

    /**
     * Remove laboratory
     *
     * @param \AppBundle\Entity\Laboratory $laboratory
     */
    public function removeLaboratory(\AppBundle\Entity\Laboratory $laboratory)
    {
        $this->laboratories->removeElement($laboratory);
    }

    /**
     * Get laboratories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLaboratories()
    {
        return $this->laboratories;
    }
}
