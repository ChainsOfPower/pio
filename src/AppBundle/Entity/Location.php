<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Location
 *
 * @ORM\Table(name="location")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LocationRepository")
 */
class Location
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="spot", type="string", length=128)
     */
    private $spot;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Organization", mappedBy="location")
     */
    private $organizations;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Laboratory", mappedBy="location", cascade={"remove"})
     */
    private $laboratories;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Location
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set spot
     *
     * @param string $spot
     *
     * @return Location
     */
    public function setSpot($spot)
    {
        $this->spot = $spot;

        return $this;
    }

    /**
     * Get spot
     *
     * @return string
     */
    public function getSpot()
    {
        return $this->spot;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Location
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->organizations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add organization
     *
     * @param \AppBundle\Entity\Organization $organization
     *
     * @return Location
     */
    public function addOrganization(\AppBundle\Entity\Organization $organization)
    {
        $this->organizations[] = $organization;

        return $this;
    }

    /**
     * Remove organization
     *
     * @param \AppBundle\Entity\Organization $organization
     */
    public function removeOrganization(\AppBundle\Entity\Organization $organization)
    {
        $this->organizations->removeElement($organization);
    }

    /**
     * Get organizations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrganizations()
    {
        return $this->organizations;
    }

    /**
     * Add laboratory
     *
     * @param \AppBundle\Entity\Laboratory $laboratory
     *
     * @return Location
     */
    public function addLaboratory(\AppBundle\Entity\Laboratory $laboratory)
    {
        $this->laboratories[] = $laboratory;

        return $this;
    }

    /**
     * Remove laboratory
     *
     * @param \AppBundle\Entity\Laboratory $laboratory
     */
    public function removeLaboratory(\AppBundle\Entity\Laboratory $laboratory)
    {
        $this->laboratories->removeElement($laboratory);
    }

    /**
     * Get laboratories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLaboratories()
    {
        return $this->laboratories;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
