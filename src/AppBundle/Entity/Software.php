<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Software
 *
 * @ORM\Table(name="software")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SoftwareRepository")
 */
class Software
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="contactPerson", type="string", length=128, nullable=true)
     */
    private $contactPerson;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expirationDate", type="datetime")
     * @Assert\DateTime(message="Morate unijeti ispravan datum")
     */
    private $expirationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="serialNumber", type="string", length=128)
     * @Assert\NotBlank(message="Morate unijeti serijski broj")
     */
    private $serialNumber;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Owner", inversedBy="softwares")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\HardwareSoftware", mappedBy="softwareId", cascade={"remove"})
     */
    private $hardwares;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Software
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Software
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set contactPerson
     *
     * @param string $contactPerson
     *
     * @return Software
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * Get contactPerson
     *
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Set expirationDate
     *
     * @param \DateTime $expirationDate
     *
     * @return Software
     */
    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    /**
     * Get expirationDate
     *
     * @return \DateTime
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * Set serialNumber
     *
     * @param string $serialNumber
     *
     * @return Software
     */
    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    /**
     * Get serialNumber
     *
     * @return string
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\Owner $owner
     *
     * @return Software
     */
    public function setOwner(\AppBundle\Entity\Owner $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\Owner
     */
    public function getOwner()
    {
        return $this->owner;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->hardwares = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add hardware
     *
     * @param \AppBundle\Entity\HardwareSoftware $hardware
     *
     * @return Software
     */
    public function addHardware(\AppBundle\Entity\HardwareSoftware $hardware)
    {
        $this->hardwares[] = $hardware;

        return $this;
    }

    /**
     * Remove hardware
     *
     * @param \AppBundle\Entity\HardwareSoftware $hardware
     */
    public function removeHardware(\AppBundle\Entity\HardwareSoftware $hardware)
    {
        $this->hardwares->removeElement($hardware);
    }

    /**
     * Get hardwares
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHardwares()
    {
        return $this->hardwares;
    }
}
