<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HardwareSoftware
 *
 * @ORM\Table(name="hardware_software")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HardwareSoftwareRepository")
 */
class HardwareSoftware
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Hardware", inversedBy="softwares")
     * @ORM\JoinColumn(name="hardware_id", referencedColumnName="id")
     */
    private $hardwareId;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Software", inversedBy="hardwares")
     * @ORM\JoinColumn(name="software_id", referencedColumnName="id")
     */
    private $softwareId;

    /**
     * Set hardwareId
     *
     * @param \AppBundle\Entity\Hardware $hardwareId
     *
     * @return HardwareSoftware
     */
    public function setHardwareId(\AppBundle\Entity\Hardware $hardwareId)
    {
        $this->hardwareId = $hardwareId;

        return $this;
    }

    /**
     * Get hardwareId
     *
     * @return \AppBundle\Entity\Hardware
     */
    public function getHardwareId()
    {
        return $this->hardwareId;
    }

    /**
     * Set softwareId
     *
     * @param \AppBundle\Entity\Software $softwareId
     *
     * @return HardwareSoftware
     */
    public function setSoftwareId(\AppBundle\Entity\Software $softwareId)
    {
        $this->softwareId = $softwareId;

        return $this;
    }

    /**
     * Get softwareId
     *
     * @return \AppBundle\Entity\Software
     */
    public function getSoftwareId()
    {
        return $this->softwareId;
    }
}
