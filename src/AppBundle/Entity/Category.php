<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Hardware", mappedBy="category")
     */
    private $hardwares;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Category
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->hardwares = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add hardware
     *
     * @param \AppBundle\Entity\Hardware $hardware
     *
     * @return Category
     */
    public function addHardware(\AppBundle\Entity\Hardware $hardware)
    {
        $this->hardwares[] = $hardware;

        return $this;
    }

    /**
     * Remove hardware
     *
     * @param \AppBundle\Entity\Hardware $hardware
     */
    public function removeHardware(\AppBundle\Entity\Hardware $hardware)
    {
        $this->hardwares->removeElement($hardware);
    }

    /**
     * Get hardwares
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHardwares()
    {
        return $this->hardwares;
    }
}
