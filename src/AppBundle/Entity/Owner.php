<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Owner
 *
 * @ORM\Table(name="owner")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OwnerRepository")
 */
class Owner
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Hardware", mappedBy="owner")
     */
    private $hardwares;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Software", mappedBy="owner")
     */
    private $softwares;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Owner
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Owner
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->hardwares = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add hardware
     *
     * @param \AppBundle\Entity\Hardware $hardware
     *
     * @return Owner
     */
    public function addHardware(\AppBundle\Entity\Hardware $hardware)
    {
        $this->hardwares[] = $hardware;

        return $this;
    }

    /**
     * Remove hardware
     *
     * @param \AppBundle\Entity\Hardware $hardware
     */
    public function removeHardware(\AppBundle\Entity\Hardware $hardware)
    {
        $this->hardwares->removeElement($hardware);
    }

    /**
     * Get hardwares
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHardwares()
    {
        return $this->hardwares;
    }

    /**
     * Add software
     *
     * @param \AppBundle\Entity\Software $software
     *
     * @return Owner
     */
    public function addSoftware(\AppBundle\Entity\Software $software)
    {
        $this->softwares[] = $software;

        return $this;
    }

    /**
     * Remove software
     *
     * @param \AppBundle\Entity\Software $software
     */
    public function removeSoftware(\AppBundle\Entity\Software $software)
    {
        $this->softwares->removeElement($software);
    }

    /**
     * Get softwares
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSoftwares()
    {
        return $this->softwares;
    }
}
