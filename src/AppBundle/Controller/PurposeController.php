<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Purpose;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Purpose controller.
 *
 * @Route("purpose")
 * @Security("has_role('ROLE_ADMIN')")
 */
class PurposeController extends Controller
{
    /**
     * Lists all purpose entities.
     *
     * @Route("/", name="purpose_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $purposes = $em->getRepository('AppBundle:Purpose')->findAll();

        return $this->render('purpose/index.html.twig', array(
            'purposes' => $purposes,
        ));
    }

    /**
     * Creates a new purpose entity.
     *
     * @Route("/new", name="purpose_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $purpose = new Purpose();
        $form = $this->createForm('AppBundle\Form\PurposeType', $purpose);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($purpose);
            $em->flush($purpose);

            return $this->redirectToRoute('purpose_show', array('id' => $purpose->getId()));
        }

        return $this->render('purpose/new.html.twig', array(
            'purpose' => $purpose,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a purpose entity.
     *
     * @Route("/{id}", name="purpose_show")
     * @Method("GET")
     */
    public function showAction(Purpose $purpose)
    {
        $deleteForm = $this->createDeleteForm($purpose);

        return $this->render('purpose/show.html.twig', array(
            'purpose' => $purpose,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing purpose entity.
     *
     * @Route("/{id}/edit", name="purpose_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Purpose $purpose)
    {
        $deleteForm = $this->createDeleteForm($purpose);
        $editForm = $this->createForm('AppBundle\Form\PurposeType', $purpose);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('purpose_index');
        }

        return $this->render('purpose/edit.html.twig', array(
            'purpose' => $purpose,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a purpose entity.
     *
     * @Route("/{id}", name="purpose_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Purpose $purpose)
    {
        $form = $this->createDeleteForm($purpose);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($purpose);
            $em->flush($purpose);
        }

        return $this->redirectToRoute('purpose_index');
    }

    /**
     * Creates a form to delete a purpose entity.
     *
     * @param Purpose $purpose The purpose entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Purpose $purpose)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('purpose_delete', array('id' => $purpose->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
