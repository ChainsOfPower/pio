<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 17.1.2017.
 * Time: 5:13
 */

namespace UserBundle\Controller;

use FOS\UserBundle\Model\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\ResettingController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;

class ResettingController extends BaseController
{
    public function requestAction()
    {
        if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            return new RedirectResponse($this->get('router')->generate('homepage'));
        }

        return parent::requestAction();
    }
}