<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 17.1.2017.
 * Time: 5:27
 */

namespace UserBundle\Controller;

use FOS\UserBundle\Model\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;

class SecurityController extends BaseController
{
    public function loginAction(Request $request)
    {
        if($this->get('security.authorization_checker')->isGranted('ROLE_USER'))
        {
            return new RedirectResponse($this->get('router')->generate('homepage'));
        }

        return parent::loginAction($request);
    }
}