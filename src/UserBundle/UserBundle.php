<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 17.1.2017.
 * Time: 5:01
 */

namespace UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class UserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}